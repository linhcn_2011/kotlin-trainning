package com.linhcn.kotlinnoteapp.core.data.repositories

import com.linhcn.kotlinnoteapp.core.data.local.NoteDataResource
import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

class NoteRepositoryImpl(private val noteDataResource: NoteDataResource) : NoteRepository {

    override suspend fun createNote(note: Note, user: User) = noteDataResource.create(user, note)

    override suspend fun readAllNote(user: User): List<Note> = noteDataResource.readAll(user)

    override suspend fun readNote(noteId: String, user: User): Note =
        noteDataResource.readNote(noteId, user)

    override suspend fun updateNote(note: Note, user: User) = noteDataResource.update(user, note)

    override suspend fun deleteNote(note: Note, user: User) = noteDataResource.delete(user, note)

}
