package com.linhcn.kotlinnoteapp.core.domain

import androidx.room.*

@Entity(
    tableName = "note",
    indices = [Index(value = ["id"], name = "indexNote", unique = true)],
    foreignKeys = [ForeignKey(
        entity = User::class,
        parentColumns = ["id"],
        childColumns = ["userId"]
    )]
)
class Note(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "description")
    val description: String,

    @ColumnInfo(name = "userId")
    val userId: String
)
