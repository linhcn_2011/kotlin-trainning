package com.linhcn.kotlinnoteapp.core.data.local

import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

interface NoteDataResource {

    suspend fun create(user: User, note: Note)

    suspend fun readAll(user: User): List<Note>

    suspend fun readNote(noteId: String, user: User): Note

    suspend fun update(user: User, note: Note)

    suspend fun delete(user: User, note: Note)

}