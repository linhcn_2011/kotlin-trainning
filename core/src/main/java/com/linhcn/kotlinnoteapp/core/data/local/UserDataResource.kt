package com.linhcn.kotlinnoteapp.core.data.local

import com.linhcn.kotlinnoteapp.core.domain.User

interface UserDataResource {

    suspend fun create(user: User)

    suspend fun read(user: User) : User

    suspend fun update(user: User)

    suspend fun delete(user: User)
}
