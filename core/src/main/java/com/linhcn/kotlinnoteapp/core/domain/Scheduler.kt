package com.linhcn.kotlinnoteapp.core.domain

import androidx.room.*

@Entity(
    tableName = "scheduler",
    indices = [Index(value = ["id"], name = "indexScheduler", unique = true)],
    foreignKeys = [ForeignKey(
        entity = Note::class,
        parentColumns = ["id"],
        childColumns = ["noteId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class Scheduler(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "time")
    val time: Long,

    @ColumnInfo(name = "status")
    val status: String,

    @ColumnInfo(name = "noteId")
    val noteId: String
) {

    enum class Status {
        REPEAST,
        ONE_TIME
    }
}
