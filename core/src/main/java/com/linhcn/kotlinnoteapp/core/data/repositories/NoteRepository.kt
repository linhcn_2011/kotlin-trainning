package com.linhcn.kotlinnoteapp.core.data.repositories

import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

interface NoteRepository {

    suspend fun createNote(note: Note, user: User)

    suspend fun readAllNote(user: User) : List<Note>

    suspend fun readNote(noteId: String, user: User) : Note

    suspend fun updateNote(note: Note, user: User)

    suspend fun deleteNote(note: Note, user: User)

}