package com.linhcn.kotlinnoteapp.core.data.local

import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.Scheduler

interface SchedulerDataResource {

    suspend fun create(note: Note, scheduler: Scheduler)

    suspend fun read(note: Note) : Scheduler

    suspend fun update(note: Note, scheduler: Scheduler)
}
