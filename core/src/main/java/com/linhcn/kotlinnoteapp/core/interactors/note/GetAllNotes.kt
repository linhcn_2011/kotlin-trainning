package com.linhcn.kotlinnoteapp.core.interactors.note

import com.linhcn.kotlinnoteapp.core.data.repositories.NoteRepository
import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

class GetAllNotes(private val userRepository: NoteRepository) {
    suspend operator fun invoke(user: User): List<Note> = userRepository.readAllNote(user)
}