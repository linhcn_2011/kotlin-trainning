package com.linhcn.kotlinnoteapp.core.interactors.note

import com.linhcn.kotlinnoteapp.core.data.repositories.NoteRepository
import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

class UpdateNote(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(note: Note, user: User) {
        noteRepository.updateNote(note, user)
    }
}
