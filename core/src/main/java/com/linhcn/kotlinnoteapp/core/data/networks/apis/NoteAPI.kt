package com.linhcn.kotlinnoteapp.core.data.networks.apis

import com.linhcn.kotlinnoteapp.core.data.networks.requestes.CreateNoteRequestBody
import com.linhcn.kotlinnoteapp.core.data.networks.requestes.DeleteNoteRequestBody
import com.linhcn.kotlinnoteapp.core.data.networks.requestes.ReadAllNoteRequestBody
import com.linhcn.kotlinnoteapp.core.data.networks.requestes.UpdateNoteRequestBody
import com.linhcn.kotlinnoteapp.core.data.networks.responses.BaseResponse
import com.linhcn.kotlinnoteapp.core.data.networks.responses.ReadAllNoteResponse

interface NoteAPI {

    suspend fun create(requestBody: CreateNoteRequestBody): BaseResponse

    suspend fun readAll(requestBody: ReadAllNoteRequestBody): ReadAllNoteResponse

    suspend fun update(requestBody: UpdateNoteRequestBody): BaseResponse

    suspend fun delete(requestBody: DeleteNoteRequestBody): BaseResponse
}