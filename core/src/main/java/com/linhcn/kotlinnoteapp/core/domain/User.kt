package com.linhcn.kotlinnoteapp.core.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "user", indices = [Index(value = ["id"], name = "indexUser", unique = true)])
data class User(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String,

    @ColumnInfo(name = "name")
    val username: String,

    @ColumnInfo(name = "fullName")
    val fullName: String
)