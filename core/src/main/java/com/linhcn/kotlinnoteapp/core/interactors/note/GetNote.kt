package com.linhcn.kotlinnoteapp.core.interactors.note

import com.linhcn.kotlinnoteapp.core.data.repositories.NoteRepository
import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

class GetNote(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(noteId: String, user: User): Note {
        return noteRepository.readNote(noteId, user)
    }
}