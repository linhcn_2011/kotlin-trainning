package com.linhcn.kotlinnoteapp

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.linhcn.kotlinnoteapp.frameworks.room.daos.NoteDao
import com.linhcn.kotlinnoteapp.frameworks.room.database.NoteDatabase
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NoteDaoTest {

    private lateinit var database: NoteDatabase
    private lateinit var noteDao: NoteDao

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getTargetContext()
        try {
            database = Room.inMemoryDatabaseBuilder(context, NoteDatabase::class.java)
                .allowMainThreadQueries().build()
            Log.i("init database success", database.toString())
        } catch (e: Exception) {
            Log.i("test", e.message)
        }
        noteDao = database.noteDao()
    }

    @After
    fun tearDown() {
        database.close()
    }
}