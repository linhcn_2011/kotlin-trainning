package com.linhcn.kotlinnoteapp.frameworks.room.daos

import androidx.room.*
import com.linhcn.kotlinnoteapp.core.domain.Scheduler

@Dao
interface SchedulerDao {

    @Query("SELECT * FROM scheduler WHERE scheduler.noteId =:noteId")
    fun getAll(noteId: String): List<Scheduler>

    @Query("SELECT * FROM scheduler WHERE scheduler.id = :id")
    fun getNoteScheduler(id: String): Scheduler

    @Insert
    fun insertAll(schedulers: List<Scheduler>)

    @Insert
    fun insert(scheduler: Scheduler)

    @Delete
    fun deleteAll(schedulers: List<Scheduler>)

    @Delete
    fun delete(scheduler: Scheduler)

    @Update
    fun updateAll(schedulers: List<Scheduler>)

    @Update
    fun update(scheduler: Scheduler)
}
