package com.linhcn.kotlinnoteapp.frameworks.datasources

import com.linhcn.kotlinnoteapp.core.data.local.NoteDataResource
import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.User

class NoteDataSourceImpl : NoteDataResource {

    override suspend fun create(user: User, note: Note) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun readAll(user: User): List<Note> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun readNote(noteId: String, user: User): Note {
        return Note("", "", "", "")
    }

    override suspend fun update(user: User, note: Note) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun delete(user: User, note: Note) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
