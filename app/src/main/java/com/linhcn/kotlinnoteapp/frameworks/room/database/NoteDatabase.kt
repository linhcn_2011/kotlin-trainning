package com.linhcn.kotlinnoteapp.frameworks.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.linhcn.kotlinnoteapp.core.domain.Note
import com.linhcn.kotlinnoteapp.core.domain.Scheduler
import com.linhcn.kotlinnoteapp.core.domain.User
import com.linhcn.kotlinnoteapp.frameworks.room.daos.NoteDao
import com.linhcn.kotlinnoteapp.frameworks.room.daos.SchedulerDao
import com.linhcn.kotlinnoteapp.frameworks.room.daos.UserDao

@Database(entities = [Note::class, Scheduler::class, User::class], version = 1)
abstract class NoteDatabase : RoomDatabase() {
    abstract fun noteDao(): NoteDao
    abstract fun noteSchedulerDao(): SchedulerDao
    abstract fun userDao(): UserDao
}
