package com.linhcn.kotlinnoteapp.frameworks.room.daos

import androidx.room.*
import com.linhcn.kotlinnoteapp.core.domain.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user WHERE user.id = :id")
    fun getUser(id: String): User

    @Insert
    fun insert(note: User)

    @Delete
    fun delete(note: User)

    @Update
    fun update(note: User)
}
