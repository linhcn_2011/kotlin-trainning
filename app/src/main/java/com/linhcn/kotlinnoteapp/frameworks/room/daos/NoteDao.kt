package com.linhcn.kotlinnoteapp.frameworks.room.daos

import androidx.room.*
import com.linhcn.kotlinnoteapp.core.domain.Note

@Dao
interface NoteDao {

    @Query("SELECT * FROM note")
    fun getAll(): List<Note>

    @Query("SELECT * FROM note WHERE Note.id = :id")
    fun getNote(id: String): Note

    @Insert
    fun insertAll(list: List<Note>)

    @Insert
    fun insert(note: Note)

    @Delete
    fun deleteAll(notes: List<Note>)

    @Delete
    fun delete(note: Note)

    @Update
    fun updateAll(notes: List<Note>)

    @Update
    fun update(note: Note)
}
