package com.linhcn.kotlinnoteapp.presentation.note

import com.linhcn.kotlinnoteapp.presentation.base.FragmentHolder

class CreateNoteNavigator(fragmentHolder: FragmentHolder<CreateNoteFragment>) {

    private val holder: FragmentHolder<CreateNoteFragment> = fragmentHolder

    fun backToMain() {
        holder.reference.activity?.finish()
    }
}