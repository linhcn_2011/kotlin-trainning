package com.linhcn.kotlinnoteapp.presentation.note

import com.linhcn.kotlinnoteapp.R
import com.linhcn.kotlinnoteapp.presentation.base.BaseFragment

class CreateNoteFragment(
    private val navigator: CreateNoteNavigator
) : BaseFragment() {

    override fun initLayout(): Int {
        return R.layout.fragment_create_note
    }

}