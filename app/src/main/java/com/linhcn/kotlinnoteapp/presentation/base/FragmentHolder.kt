package com.linhcn.kotlinnoteapp.presentation.base

class FragmentHolder<T : Any> {

    private var referenceHolder: T? = null

    var reference: T
        get() = requireNotNull(referenceHolder)
        set(value) {
            require(referenceHolder == null)
            referenceHolder = value
        }
}