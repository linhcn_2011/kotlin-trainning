package com.linhcn.kotlinnoteapp.presentation.note

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.linhcn.kotlinnoteapp.R
import com.linhcn.kotlinnoteapp.presentation.base.BaseBackActivity


class CreateNoteActivity : BaseBackActivity() {

    override fun initTitle(): Int {
        return R.string.txt_title_create_note_activity
    }

    override fun initFragmentContent(): Class<out Fragment> {
        return CreateNoteFragment::class.java
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}