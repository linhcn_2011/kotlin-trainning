package com.linhcn.kotlinnoteapp.presentation.common.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatEditText
import com.linhcn.kotlinnoteapp.presentation.common.utils.hideKeyboard
import com.linhcn.kotlinnoteapp.presentation.common.utils.showKeyboard

class EditText(context: Context?, attrs: AttributeSet?) :
    AppCompatEditText(context, attrs), View.OnFocusChangeListener {

    val focusChangeListener: OnFocusChangeListener? = null

    init {
        onFocusChangeListener = this
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        focusChangeListener?.onFocusChange(v, hasFocus)
        if (v != null) {
            if (!hasFocus) {
                hideKeyboard(v)
            }
//            else {
//                showKeyboard(v)
//            }
        }
    }
}