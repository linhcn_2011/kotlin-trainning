package com.linhcn.kotlinnoteapp.presentation.common.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.linhcn.kotlinnoteapp.presentation.common.view.EditText

fun EditText.hideKeyboard(v: View) {
    val imm: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(v.windowToken, 0)
}

fun EditText.showKeyboard(v: View) {
    val imm: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(v, 0)
}