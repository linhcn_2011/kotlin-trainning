package com.linhcn.kotlinnoteapp.presentation.note

import com.linhcn.kotlinnoteapp.presentation.base.FragmentComponent
import com.linhcn.kotlinnoteapp.presentation.base.FragmentHolder

class CreateNoteComponent : FragmentComponent<CreateNoteFragment> {

    override fun getFragment(): CreateNoteFragment {
        val fragmentHolder: FragmentHolder<CreateNoteFragment> = FragmentHolder()
        val navigator = CreateNoteNavigator(fragmentHolder)
        return CreateNoteFragment(navigator).also { fragmentHolder.reference = it }
    }

    class CreateNoteFactory : FragmentComponent.Factory<CreateNoteComponent> {
        override fun create(): CreateNoteComponent {
            return CreateNoteComponent()
        }
    }
}