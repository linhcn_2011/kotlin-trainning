package com.linhcn.kotlinnoteapp.presentation.common.view

import android.content.Context
import android.util.AttributeSet
import androidx.cardview.widget.CardView
import com.linhcn.kotlinnoteapp.R

class NormalCardView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        cardElevation = context.resources.getDimension(R.dimen.elevationNormal)
        radius = context.resources.getDimension(R.dimen.radiusNormal)
    }
}