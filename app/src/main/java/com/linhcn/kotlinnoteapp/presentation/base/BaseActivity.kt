package com.linhcn.kotlinnoteapp.presentation.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.linhcn.kotlinnoteapp.R
import com.linhcn.kotlinnoteapp.presentation.AppFragmentFactory
import org.koin.android.ext.android.inject

abstract class BaseActivity : FragmentActivity() {

    private val fragmentFactory: AppFragmentFactory by inject()

    @LayoutRes
    abstract fun initLayout(): Int

    abstract fun initFragmentContent(): Class<out Fragment>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachFragmentContent(initFragmentContent())
    }

    private fun attachFragmentContent(clazz: Class<out Fragment>) {
        supportFragmentManager.fragmentFactory = fragmentFactory
        supportFragmentManager.beginTransaction().add(
            R.id.fragment_container_view,
            fragmentFactory.instantiate(classLoader, clazz.name)
        ).commit()
    }
}
