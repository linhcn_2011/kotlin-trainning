package com.linhcn.kotlinnoteapp.presentation.common.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.linhcn.kotlinnoteapp.R
import com.linhcn.kotlinnoteapp.databinding.ItemDayWeekBinding
import com.linhcn.kotlinnoteapp.databinding.ViewDayWeekBinding


class DayWeekView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    // TODO
//    var dayWeekViewHolderListener: DayWeekViewHolder.Listener? = null
//    set(value) {
//        binding.
//    }

    lateinit var binding: ViewDayWeekBinding

    private var dayWeekAdapter: DayWeekAdapter
    private lateinit var dayWeekItems: ArrayList<DayWeekItem>

    init {
        val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.view_day_week, this, true)

        binding.rcvDayInWeek.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.rcvDayInWeek.adapter =
            DayWeekAdapter(initDayWeeksItems()).also { dayWeekAdapter = it }
    }

    private fun initDayWeeksItems(): ArrayList<DayWeekItem> {
        val array: Array<String> =
            context.resources.getStringArray(R.array.dayInWeeks) as Array<String>
        return DayWeekItem.convertFromArrayResource(array).also { dayWeekItems = it }
    }

    // adapter
    inner class DayWeekAdapter(private val dateWeekItems: ArrayList<DayWeekItem>) :
        RecyclerView.Adapter<DayWeekViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayWeekViewHolder {
            val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
            val itemDayWeekBinding: ItemDayWeekBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_day_week, parent, false)
            return DayWeekViewHolder(itemDayWeekBinding)
        }

        override fun getItemCount(): Int {
            return dateWeekItems.size
        }

        override fun onBindViewHolder(holder: DayWeekViewHolder, position: Int) {
            holder.bind(dateWeekItems[position])
        }
    }

    // view holder
    class DayWeekViewHolder(private val binding: ItemDayWeekBinding) :
        RecyclerView.ViewHolder(binding.root) {

        var listener: Listener? = null
            set(value) {
                binding.listener = value
                field = value
            }

        fun bind(item: DayWeekItem) {
            binding.item = item
            binding.executePendingBindings()
        }

        interface Listener {
            fun onClick(dayWeekItem: DayWeekItem)
        }
    }

    // data type
    data class DayWeekItem(@Bindable private var value: String, @Bindable private var selected: Boolean) :
        BaseObservable() {

        companion object {
            fun convertFromArrayResource(arrayString: Array<String>): ArrayList<DayWeekItem> {
                val dayWeekItems: ArrayList<DayWeekItem> = ArrayList()
                for (item: String in arrayString) {
                    dayWeekItems.add(DayWeekItem(item, false))
                }
                return dayWeekItems
            }
        }
    }
}