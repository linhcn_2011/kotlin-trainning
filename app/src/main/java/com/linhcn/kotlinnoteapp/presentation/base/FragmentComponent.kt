package com.linhcn.kotlinnoteapp.presentation.base

import androidx.fragment.app.Fragment

/**
 * Contain the fragment dependencies
 */
interface FragmentComponent<T : Fragment> {

    /**
     * Use for init fragment and the fragment dependencies
     */
    fun getFragment() : T

    /**
     * Use for create FragmentComponent
     */
    interface Factory<T : FragmentComponent<out Fragment>> {
        fun create(): T
    }
}