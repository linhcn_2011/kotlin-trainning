package com.linhcn.kotlinnoteapp.presentation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.linhcn.kotlinnoteapp.presentation.base.FragmentComponent

class AppFragmentFactory(
    private val providers: Map<Class<out Fragment>, FragmentComponent.Factory<*>>
) : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        val clazz = loadFragmentClass(classLoader, className)
        return providers.entries
           .find {
               it.key.isAssignableFrom(clazz)
           }?.value?.create()?.getFragment()
           ?: super.instantiate(classLoader, className)
    }

}