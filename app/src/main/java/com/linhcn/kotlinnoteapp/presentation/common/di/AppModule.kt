package com.linhcn.kotlinnoteapp.presentation.common.di

import androidx.fragment.app.Fragment
import com.linhcn.kotlinnoteapp.presentation.AppFragmentFactory
import com.linhcn.kotlinnoteapp.presentation.base.FragmentComponent
import com.linhcn.kotlinnoteapp.presentation.note.CreateNoteComponent
import com.linhcn.kotlinnoteapp.presentation.note.CreateNoteFragment
import org.koin.dsl.module

val fragmentModule = module {

    single {
        val providers = HashMap<Class<out Fragment>, FragmentComponent.Factory<*>>()
        providers[CreateNoteFragment::class.java] = CreateNoteComponent.CreateNoteFactory()
        AppFragmentFactory(providers)
    }

}