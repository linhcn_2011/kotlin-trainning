package com.linhcn.kotlinnoteapp.presentation.common.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.cardview.widget.CardView
import androidx.databinding.DataBindingUtil
import com.linhcn.kotlinnoteapp.R
import com.linhcn.kotlinnoteapp.databinding.ViewActionBarBackBinding

class BackActionBarView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    var layoutBinding: ViewActionBarBackBinding

    var onAppBackActionBarListener: OnAppBackActionBarListener? = null
        set(value) {
            layoutBinding.listener = value
            field = value
        }

    var title: String? = null
        set(value) {
            layoutBinding.title = value
            field = value
        }

    interface OnAppBackActionBarListener {
        fun onClickBackActivity(view: View)
    }

    init {
        val layoutInflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        layoutBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.view_action_bar_back, this, true)
    }
}

