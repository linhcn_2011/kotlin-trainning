package com.linhcn.kotlinnoteapp.presentation.base

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import com.linhcn.kotlinnoteapp.R
import com.linhcn.kotlinnoteapp.databinding.ActivityBaseBackBinding
import com.linhcn.kotlinnoteapp.presentation.common.view.BackActionBarView.OnAppBackActionBarListener

abstract class BaseBackActivity : BaseActivity(), OnAppBackActionBarListener {

    @StringRes
    abstract fun initTitle(): Int

    override fun initLayout(): Int {
        return R.layout.activity_base_back
    }

    override fun onClickBackActivity(view: View) {
        onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initActivityBinding()
        super.onCreate(savedInstanceState)
    }

    private fun initActivityBinding() {
        val activityBinding: ActivityBaseBackBinding =
            DataBindingUtil.setContentView(this, initLayout())
        activityBinding.actionBarListener = this
        activityBinding.title = getString(initTitle())
    }
}
