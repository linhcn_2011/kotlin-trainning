package com.linhcn.kotlinnoteapp

import android.app.Application
import androidx.room.Room
import com.linhcn.kotlinnoteapp.frameworks.room.database.NoteDatabase
import com.linhcn.kotlinnoteapp.presentation.common.di.fragmentModule
import org.koin.core.context.startKoin

class NoteApp : Application() {

    companion object {
        const val databaseName = "note"
        var database: NoteDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        initDataBase()
        initDI()
    }

    private fun initDI() {
        startKoin {
            modules(fragmentModule)
        }
    }

    private fun initDataBase() {
        database = Room
            .databaseBuilder(
                this,
                NoteDatabase::class.java,
                databaseName
            ).build()
    }
}
